## intro!
**Listen up**, this is not an original project. This is a Ruby rewrite from original *vircar* (which is also [open source](https://github.com/dn5/vircar) project) written in C. A car engine built for hackers so they can experiment with CAN Bus. Read on.

## vircar
**vircar.rb** (**vir**-tual-**car**) is a car written in Ruby that opens a CAN Bus network on your computer and register operations that should be sent to the ECU, just like a real car. This software is licensed under GNU General Public V3 license. Please, keep the software open source and contribute to the project if you want. I wrote this software for educational and pro-bono purpose only.  
  
## a car? really?
Yes! A fully, functional car. Well, not really no. It does represent a car but in a limited spirit and form. I was writing a post about [cyber-attacks](http://dn5.ljuska.org/napadi-na-auto-sistem-1.html) on a vehicle systems and [another one](http://dn5.ljuska.org/cyber-attacks-on-vehicles-2.html) that show (original) **vircar** in action, and didn't have necessary (hardware) equipment to test security, so *vircar* is born. It allows me to test and experiment with a fuzzing technique on a virtual car. You may want to implement other functions and options which you can do over **car.rb** file inside `./src/car.rb`.  
  
## functionality
Currently, **vircar.rb** offers several operations (the one that original vircar offer) that are registered and reserved on ECU. These are: **ENON** (Engine on), **ENOF** (Engine off), **LOCK** (Lock doors), **DOCK** (Unlock doors), and **KILL** (destroy the car, remove virtual bus). As I said, you may want to implement other functions in the car, so be free to tune it however you like it. Also, if you've made a NOS or something cool make a pull request, I would be glad to see contributors to the project.  
  
## requirements
* `→ gem install --user-install bindata`
* Yet to implmenet?!
  
## compile & running
To clone the repo and compile it from source: 

	$ git clone https://gitlab.com/duraki/vircar.rb.git
	$ cd vircar.rb
	$ ruby src/vircar.rb

To run vircar use sudo (entierly for CAN bus device linking)	

	$ sudo ./vircar.rb
	Welcome to vir(tual) car.
	~
	/ ** /
	=========================================
	https://gitlab.com/duraki/vircar.rb

	# waiting for operation

To **exit** vircar use Ctrl+C.  
To **kill** a car and remove protocol use:
	
	$ ./vircar.rb k
	ka-boom, pfw, aaa, ts
	*car exploded*

## outro
If you are interested in car hacking, write me on [twitter](https://twitter.com/dn5__). Any additional options and ECU operations should be applied to pull requests on this official repository. Please, keep the code as clean as possible. Thanks to original supporters for also original [vircar](https://github.com/dn5/vircar).