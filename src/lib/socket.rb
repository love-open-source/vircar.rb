require 'bindata'
require 'socket'

class CanSocket < Socket

  PF_CAN=29
  AF_CAN=PF_CAN
  CAN_RAW=1
  SIOCGIFINDEX=0x8933

  # https://github.com/st3fan/osx-10.9/blob/master/libpcap-42/libpcap/pcap-can-linux.c
  # https://docs.python.org/3/library/socket.html#socket.PF_CAN
  # http://src.gnu-darwin.org/src/sys/pc98/pc98/canbus.c.html
  # https://www.kernel.org/doc/Documentation/networking/can.txt
  
  def initialize(can_if_name)
    super(PF_CAN, Socket::SOCK_RAW, CAN_RAW)
    
    if_idx_req = can_if_name.ljust(16, "\0")+[0].pack("L")
    ioctl(SIOCGIFINDEX, if_idx_req)

    if_name, if_index = if_idx_req.unpack("A16L")

    sockaddr_can = [AF_CAN,if_index,0,0,0].pack("SlLLS")
    bind(sockaddr_can)    
  end

end