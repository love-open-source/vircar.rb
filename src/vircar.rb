# 
# @author		Halis Duraki <duraki@null.net>
# @project		vircar.rb (Virtual car / Rewrite from vircar.c)
# @package		love-open-source/vircar.rb@gitlab
# @website		http://dn5.ljuska.org
# 

# This is a rewrite of the original vircar.c file which 
# officially holds almost whole vircar engine with couple 
# of dependencies here and there.

# Always above the sky.

require_relative('car.rb')
require_relative('lib/socket.rb')

class Car

	engine = "vircar"

	@socket = CanSocket.new("box")

	def print_head
		puts "Welcome to vir(tual) car. A Ruby rewrite of the original"
		puts "vircar.c engine done in C. Check out more information about"
		puts "this topic on my blog!"
		puts "+"
		puts "https://gitlab.com/love-open-source/vircar.rb"
	end

	def create_car

	end

end